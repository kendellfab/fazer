// Fazer provides a manifest file for all pages to be rendered.
package fazer

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/pelletier/go-toml"
	"github.com/pkg/errors"
)

const (
	base            = ":base"
	defaultAssetDir = "static"
)

var ErrorSectionNotExists = errors.New("section doesn't exist")
var ErrorPageNotExists = errors.New("page doesn't exist")

// MustDecodeManifest decodes the toml representation of a manifest.
//
// [sections]
//		[sections.front]
//		base="front/base.tpl"
//		[sections.front.pages.index]
//		tpls=[":base", "front/index.tpl"]
//		title="Page Title"
//		description="Page description"
//		canonical="http://page.url"
//	[js]
//		[js.vue]
//		path="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"
//		min="https://cdn.jsdelivr.net/npm/vue"
// [css]
//		[css.bulma]
//		path=""
//		min=""
func MustDecodeManifestFile(input string, opts ...func(*Manifest) error) *Manifest {
	var m Manifest
	inputFile, err := os.Open(input)
	if err != nil {
		log.Fatal(err)
	}
	err = toml.NewDecoder(inputFile).Decode(&m)
	if err != nil {
		log.Fatal(err)
	}
	m.Finalize(opts...)
	return &m
}

// MustDecodeManifestJson decodes the json representation of a manifest.
//
// {
//  "sections": {
//    "front": {
//      "base": "front/base.tpl",
//      "pages": {
//        "index": {
//          "tpls": [
//            ":base",
//            "front/index.tpl"
//          ],
//          "title": "Page Title",
//          "description": "Page description",
//          "canonical": "http://page.url"
//        }
//		}
//	},
//	"js": {
//    	"vue": {
//      	"path": "https://cdn.jsdelivr.net/npm/vue/dist/vue.js",
//      	"min": "https://cdn.jsdelivr.net/npm/vue"
//    	}
//	},
//	"css": {
//    	"bulma": {
//      	"path": "/css/bulma.css",
//      	"no-min": true
//    	}
//	}
// }
func MustDecodeManifestJson(data string, opts ...func(*Manifest) error) *Manifest {
	input, err := os.Open(data)
	if err != nil {
		log.Fatal("error loading manifest file", err)
	}
	var m Manifest
	err = json.NewDecoder(input).Decode(&m)
	if err != nil {
		log.Fatal("error decoding manifest", err)
	}
	m.Finalize(opts...)
	return &m
}

func AssetsPath(path string) func(*Manifest) error {
	return func(m *Manifest) error {
		m.AssetDir = path
		return nil
	}
}

func SetProduction(prod bool) func(*Manifest) error {
	return func(m *Manifest) error {
		m.Production = prod
		return nil
	}
}

// Manifest is a collection of templates, broken up into sections, javascript, css, and images.
// Assets are versioned based upon the timestamp of the asset.
type Manifest struct {
	WorkingDir string
	AssetDir   string
	Production bool

	Sections map[string]Section `toml:"sections"`
	Partials map[string]string  `toml:"partials"`

	Js  map[string]Item `toml:"js"`
	Css map[string]Item `toml:"css"`
	Img map[string]Item `toml:"img"`
}

// IteratePages runs over every page, Fazer uses this to register templates.
func (m *Manifest) IteratePages() chan *Page {
	res := make(chan *Page)
	go func() {
		for s, sec := range m.Sections {
			for p, pg := range sec.Pages {
				path := formatPath(s, p)
				pg.path = path
				res <- pg
			}
		}
		close(res)
	}()
	return res
}

// formatPath takes a section and a page and turns it into the '.' seperated key
func formatPath(section, key string) string {
	return fmt.Sprintf("%s.%s", section, key)
}

// LookupPage gets the page from a section.page key.  This key is added in the fazer renderer, based upon the IteratePages path creation
func (m *Manifest) LookupPage(path string) (*Page, bool) {
	if !strings.Contains(path, ".") {
		return nil, false
	}

	parts := strings.Split(path, ".")
	sec, pg := parts[0], parts[1]
	if sec, secOk := m.Sections[sec]; secOk {
		if pg, pgOk := sec.Pages[pg]; pgOk {
			return pg, true
		}
	}
	return nil, false
}

// Finalize finishes setting up the manifest.  The woringdir and assetdir is set.  The sections setup the templates for the pages.
// Also the asset items are setup to use minified versions and appends the timestamp for caching purposes.
func (m *Manifest) Finalize(opts ...func(*Manifest) error) {
	var err error
	m.WorkingDir, err = os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	m.AssetDir = defaultAssetDir

	for _, opt := range opts {
		opt(m)
	}

	for _, sec := range m.Sections {
		for id, page := range sec.Pages {
			if page.ID == "" {
				page.ID = id
			}
			for i, tpl := range page.Tpls {
				if tpl == base {
					page.Tpls[i] = sec.Base
				}
			}
			sec.Pages[id] = page
		}
	}
	m.initItems(m.Js)
	m.initItems(m.Css)
	m.initItems(m.Img)
}

func (m *Manifest) initItems(is map[string]Item) {
	for key, item := range is {
		ver, err := m.getVersion(item.Path)
		item.Version = ver
		if item.Min == "" {
			item.Min = m.generateMinPath(item)
		}
		if err == nil {
			item.Path = m.appendVersion(item.Path, item.Version)
			if item.Min != "" {
				item.Min = m.appendVersion(item.Min, item.Version)
			}
		} else {
			log.Println("version error:", err)
		}
		is[key] = item
	}
}

func (m *Manifest) generateMinPath(i Item) string {
	if i.NoMin {
		return ""
	}

	suffix := filepath.Ext(i.Path)

	newSuffix := ".min" + suffix
	return strings.Replace(i.Path, suffix, newSuffix, 1)
}

func (m *Manifest) appendVersion(p string, v int64) string {
	return fmt.Sprintf("%s?v=%d", p, v)
}

func (m *Manifest) getVersion(path string) (int64, error) {
	p := filepath.Join(m.WorkingDir, m.AssetDir, path)
	fi, err := os.Stat(p)
	if err != nil {
		return 0, err
	}
	return fi.ModTime().Unix(), nil
}

func (m *Manifest) String() string {
	result := fmt.Sprintf("Production: %t\n", m.Production)
	for secKey, sec := range m.Sections {
		result += fmt.Sprintf("[sections.%s]\n", secKey)
		result += fmt.Sprintf("base=%s\n", sec.Base)
		for pageKey, page := range sec.Pages {
			result += fmt.Sprintf("[section.%s.pages.%s]\n", secKey, pageKey)
			result += fmt.Sprintf("tpls=%s\n", page.Tpls)
		}
	}
	result += "\n"
	result += "[js]\n"
	result += m.formatItems(m.Js)
	result += "\n"

	result += "[css]\n"
	result += m.formatItems(m.Css)

	return result
}

func (m *Manifest) formatItems(is map[string]Item) string {
	result := ""
	for key, val := range m.Js {
		result += fmt.Sprintf("[js.%s]\n", key)
		result += fmt.Sprintf("path=%s\n", val.Path)
		result += fmt.Sprintf("min=%s\n", val.Min)
		result += fmt.Sprintf("no-min=%t\n", val.NoMin)
		result += fmt.Sprintf("version=%d\n", val.Version)
	}
	return result
}

// GetJsPath is intended to be registered with the fazer template rendering.  This will return the path for the keyed item.
// This reflects the environment prod or not, returning a minified or regular path.
func (m *Manifest) GetJsPath(key string) string {
	item, ok := m.Js[key]
	if !ok {
		return ""
	}
	return m.getPath(&item)
}

// GetCssPath is intended to be registered with the fazer template rendering.  This will return the path for the keyed item.
// This reflects the environment prod or not, returning a minified or regular path.
func (m *Manifest) GetCssPath(key string) string {
	item, ok := m.Css[key]
	if !ok {
		return ""
	}
	return m.getPath(&item)
}

// GetImgPath is intended to be registered with the fazer template rendering.  This will return the path for the keyed item.
// This reflects the environment prod or not, returning a minified or regular path.
func (m *Manifest) GetImgPath(key string) string {
	item, ok := m.Img[key]
	if !ok {
		return ""
	}
	return m.getPath(&item)
}

func (m *Manifest) getPath(i *Item) string {
	p := ""
	if m.Production && i.Min != "" {
		p = i.Min
	} else {
		p = i.Path
	}

	return p
}

// CreateSection adds a section to this manifest
func (m *Manifest) CreateSection(name, baseTpl string) {
	section := Section{Base: baseTpl, Pages: make(map[string]*Page)}
	m.Sections[name] = section
}

// AddPage to a section
func (m *Manifest) AddPage(sectionName, pageName string, page *Page) error {
	sec, secOK := m.Sections[sectionName]
	if !secOK {
		return ErrorSectionNotExists
	}
	sec.Pages[pageName] = page
	return nil
}

// AddJS adds an item to the javascript map
func (m *Manifest) AddJS(key string, js Item) {
	m.Js[key] = js
}

// AddCSS adds an item to the css map
func (m *Manifest) AddCSS(key string, css Item) {
	m.Css[key] = css
}

// AddImg adds an item to the image map
func (m *Manifest) AddImg(key string, img Item) {
	m.Img[key] = img
}

// Add a partial to the manifest
func (m *Manifest) AddPartial(key, path string) {
	m.Partials[key] = path
}

// Section provides a logical grouping of templates, i.e. front and admin.  The base property, is encoded in the manifest file as :base.
// This way you can specify a base template for the section, without constantly duplicating the string.
// Pages is a map of all of the pages registered in the section.
type Section struct {
	Base  string           `toml:"base"`
	Pages map[string]*Page `toml:"pages"`
}

// Page provides a representation of a renderedable or an output html file.
// Title: this is the html meta title
// Description: this is the html meta description
// Tpls: this is an array of templates required to render this page.
// Canonical: this is the html canonical meta
type Page struct {
	ID                 string   `toml:"id"`
	path               string   `toml:"path"`
	Title              string   `toml:"title"`
	Description        string   `toml:"description"`
	Tpls               []string `toml:"tpls"`
	Canonical          string   `toml:"canonical"`
	OgURL              string   `toml:"ogurl"`
	OgTitle            string   `toml:"ogtitle"`
	OgDescription      string   `toml:"ogdescription"`
	OgImage            string   `toml:"ogimage"`
	OgImageType        string   `toml:"ogimagetype"`
	OgImageWidth       string   `toml:"ogimagewidth"`
	OgImageHeight      string   `toml:"ogimageheight"`
	FbAppID            string   `toml:"fbappid"`
	OgType             string   `toml:"ogtype"`
	OgLocale           string   `toml:"oglocale"`
	TwitterCard        string   `toml:"twittercard"`
	TwitterSite        string   `toml:"twittersite"`
	TwitterTitle       string   `toml:"twittertitle"`
	TwitterDescription string   `toml:"twitterdescription"`
	TwitterCreator     string   `toml:"twittercreator"`
	TwitterImage       string   `toml:"twitterimage"`
}

// GetTitle returns the page title, intended to be called from the template with a default value
func (p Page) GetTitle(def string) string {
	if p.Title == "" {
		return def
	}
	return p.Title
}

// GetDescription returns page description, intended to be called from the template with a default value
func (p Page) GetDescription(def string) string {
	if p.Description == "" {
		return def
	}
	return p.Description
}

// GetOgUrl returns the open graph url or the canonical url
func (p Page) GetOgUrl() string {
	if p.OgURL != "" {
		return p.OgURL
	}
	return p.Canonical
}

// GetOgTitle returns the open graph title for the page, or the regular title, or the default value passed in
func (p Page) GetOgTitle(def string) string {
	if p.OgTitle != "" {
		return p.OgTitle
	} else if p.Title != "" {
		return p.Title
	} else {
		return def
	}
}

// GetOgDescription returns the open graph description, the regular description, or the default value passed in
func (p Page) GetOgDescription(def string) string {
	if p.OgDescription != "" {
		return p.OgDescription
	} else if p.Description != "" {
		return p.Description
	} else {
		return def
	}
}

// GetOgImage returns the open graph image or the default value passed in
func (p Page) GetOgImage(def string) string {
	if p.OgImage != "" {
		return p.OgImage
	}
	return def
}

// GetFbAppId returns the fb app id or the default value passed in
func (p Page) GetFbAppId(def string) string {
	if p.FbAppID != "" {
		return p.FbAppID
	}
	return def
}

// GetOgType returns the open graph type or the default website
func (p Page) GetOgType() string {
	if p.OgType != "" {
		return p.OgType
	}
	return "website"
}

// GetOgLocale returns the open graph locale or the default locale
func (p Page) GetOgLocale() string {
	if p.OgLocale != "" {
		return p.OgLocale
	}
	return "en_US"
}

// GetTwitterCard returns the twitter card summary
func (p Page) GetTwitterCard(def string) string {
	if p.TwitterCard != "" {
		return p.TwitterCard
	} else if p.Title != "" {
		return p.Title
	}
	return def
}

// GetTwitterSite returns the twitter site or default.  i.e. @KendellFab
func (p Page) GetTwitterSite(def string) string {
	if p.TwitterSite != "" {
		return p.TwitterSite
	}
	return def
}

// GetTwitterTitle returns the twitter title or the page title or the default
func (p Page) GetTwitterTitle(def string) string {
	if p.TwitterTitle != "" {
		return p.TwitterTitle
	} else if p.Title != "" {
		return p.Title
	}
	return def
}

// GetTwitterDescription returns the twitter description or the page description or the default
func (p Page) GetTwitterDescription(def string) string {
	if p.TwitterDescription != "" {
		return p.TwitterDescription
	} else if p.Description != "" {
		return p.Description
	}
	return def
}

// GetTwitterCreator returns the twitter creator or default
func (p Page) GetTwitterCreator(def string) string {
	if p.TwitterCreator != "" {
		return p.TwitterCreator
	}
	return def
}

// GetTwitterImage returns the twitter image or default
func (p Page) GetTwitterImage(def string) string {
	if p.TwitterImage != "" {
		return p.TwitterImage
	}
	return def
}

// Item store paths for static assets [js|css|images]
type Item struct {
	Path    string `toml:"path"`
	Min     string `toml:"min"`
	NoMin   bool   `toml:"no-min" json:"no-min"`
	Version int64
}
