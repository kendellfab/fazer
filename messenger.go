package fazer

import (
	"fmt"
	htpl "html/template"
	"io"
	"log"
	"os"
	"path/filepath"
	"text/template"

	"github.com/pelletier/go-toml"
)

type MessageType int

const (
	Plain MessageType = iota
	Html
)

type Messages map[string]Message

type Message struct {
	Plain    []string `toml:"plain"`
	Html     []string `toml:"html"`
	plainTpl *template.Template
	htmlTpl  *htpl.Template
}

type Messenger struct {
	messages Messages
	tplDir   string
	prod     bool
	tplFuncs map[string]interface{}
}

func MustDecodeMessageManifest(data, tplDir string, production bool) *Messenger {
	var m Messages
	err := toml.Unmarshal([]byte(data), &m)
	if err != nil {
		log.Fatal("error decoding data", err)
	}
	messenger := &Messenger{messages: m, tplDir: tplDir, prod: production, tplFuncs: make(map[string]interface{})}
	return messenger
}

func MustDecodeMessageManifestFile(path, tplDir string, production bool) *Messenger {
	var m Messages
	inputFile, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	err = toml.NewDecoder(inputFile).Decode(&m)
	if err != nil {
		log.Fatal("error deciding file", err)
	}
	messenger := &Messenger{messages: m, tplDir: tplDir, tplFuncs: make(map[string]interface{}), prod: production}
	return messenger
}

func (m *Messenger) RegisterTemplateFunc(key string, fn interface{}) {
	m.tplFuncs[key] = fn
}

func (m *Messenger) RenderTemplate(key string, data map[string]interface{}, w io.Writer, mt MessageType) error {
	switch mt {
	case Plain:
		tpl, err := m.acquireTemplate(key)
		if err != nil {
			return fmt.Errorf("error generating plain message: %w", err)
		}
		return tpl.Execute(w, data)
	case Html:
		tpl, err := m.acquireHtmlTemplate(key)
		if err != nil {
			return fmt.Errorf("error generating Html message: %w", err)
		}
		return tpl.Execute(w, data)
	}
	return ErrorNoTemplate
}

func (mgr *Messenger) Finalize() {
	for k, m := range mgr.messages {

		var plain []string
		for _, elem := range m.Plain {
			plain = append(plain, filepath.Join(mgr.tplDir, elem))
		}
		m.Plain = plain

		var html []string
		for _, elem := range m.Html {
			html = append(html, filepath.Join(mgr.tplDir, elem))
		}
		m.Html = html

		if mgr.prod {
			if len(m.Plain) > 0 {
				if pTpl, err := mgr.parseTemplate(m.Plain...); err == nil {
					m.plainTpl = pTpl
				} else {
					log.Println("error parsing plain template", plain, err)
				}
			}

			if len(m.Html) > 0 {
				if hTpl, err := mgr.parseHtmlTemplate(m.Html...); err == nil {
					m.htmlTpl = hTpl
				} else {
					log.Println("error parsing html template", html, err)
				}
			}
		}

		mgr.messages[k] = m
	}
}

func (m *Messenger) acquireTemplate(key string) (*template.Template, error) {
	if m.prod {
		if msg, ok := m.messages[key]; ok {
			return msg.plainTpl, nil
		} else {
			return nil, ErrorNoTemplate
		}
	}
	return m.parseTemplate(m.messages[key].Plain...)
}

func (m *Messenger) acquireHtmlTemplate(key string) (*htpl.Template, error) {
	if m.prod {
		if msg, ok := m.messages[key]; ok {
			return msg.htmlTpl, nil
		} else {
			return nil, ErrorNoTemplate
		}
	}
	return m.parseHtmlTemplate(m.messages[key].Html...)
}

func (m *Messenger) parseTemplate(tpls ...string) (*template.Template, error) {
	return template.New(filepath.Base(tpls[0])).Funcs(m.tplFuncs).ParseFiles(tpls...)
}

func (m *Messenger) parseHtmlTemplate(tpls ...string) (*htpl.Template, error) {
	return htpl.New(filepath.Base(tpls[0])).Funcs(m.tplFuncs).ParseFiles(tpls...)
}
