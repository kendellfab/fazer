package fazer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/url"
	"strings"
	"time"

	"github.com/microcosm-cc/bluemonday"
	"github.com/yuin/goldmark"
	"mvdan.cc/xurls"
)

const (
	delim = "<!-- trim -->"
)

func Truncate(in string, length int) string {
	if len(in) < length {
		return in
	}
	return in[:length]
}

func MRender(in string) template.HTML {
	var buf bytes.Buffer
	err := goldmark.Convert([]byte(in), &buf)
	if err != nil {
		log.Println("error converting markdown", err)
		return template.HTML("")
	}
	return template.HTML(bluemonday.UGCPolicy().SanitizeBytes(buf.Bytes()))
}

func Trim(in string) string {
	if !strings.Contains(in, delim) {
		return in
	}

	return strings.Split(in, delim)[0]
}

func HasTrim(in string) bool {
	return strings.Contains(in, delim)
}

func XUrls(in string) template.HTML {
	urls := xurls.Relaxed.FindAllString(in, -1)
	for _, u := range urls {
		anchor := fmt.Sprintf(`<a href="%s">%s</a>`, u, u)
		in = strings.Replace(in, u, anchor, 1)
	}
	return template.HTML(in)
}

func Marshal(in interface{}) (template.JS, error) {
	a, err := json.Marshal(in)
	return template.JS(a), err
}

func BaseURL(base string) func() string {
	return func() string {
		return base
	}
}

func PathEscape(value string) string {
	return url.PathEscape(value)
}

func DateSimple(in time.Time) string {
	return in.Format("Jan 2, 2006")
}

func SafeHTML(input string) template.HTML {
	return template.HTML(input)
}
