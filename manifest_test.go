package fazer

import "testing"

const (
	MANIFEST = `[sections]
	[sections.front]
	base="front/base.html"
    [sections.front.pages.home]
    tpls=[":base", "front/index.html"]
    [sections.front.pages.portfolio]
    tpls=[":base", "front/portfolio.html"]

    [sections.admin]
    base="admin/admin.html"
    [sections.admin.pages.dashboard]
    tpls=[":base", "admin/dashboard.html"]
    [sections.admin.pages.contacts]
    tpls=[":base", "admin/contacts.html"]

[js]
    [js.foundation]
    path="/js/vendor/foundation.js"
    min="/js/vendor/foundation.min.js"
    [js.jquery]
    path="/js/vendor/jquery.js"
    min="/js/vendor/jquery.min.js"
    [js.what-input]
    path="/js/vendor/what-input.js"
    min="/js/vendor/what-input.min.js"
    [js.app]
    path="/js/app.js"
    [js.vue]
    path="/js/vue.js"
    min="/js/vue.min.js"

[css]
    [css.foundation]
    path="/css/foundation75.css"
    min="/css/foundation75.min.css"
    [css.app]
    path="/css/app.css"
	no-min=true
`
)

func TestInit(t *testing.T) {
	m := MustDecodeManifest(MANIFEST, false)
	secCount := len(m.Sections)
	if secCount != 2 {
		t.Error("Section count.  Expected 2, got", secCount)
	}

	jsCount := len(m.Js)
	if jsCount != 5 {
		t.Error("JS count.  Expected 5, got", jsCount)
	}

	cssCount := len(m.Css)
	if cssCount != 2 {
		t.Error("CSS count. Expected 2, got", cssCount)
	}
}

func TestBaseInterpolation(t *testing.T) {
	m := MustDecodeManifest(MANIFEST, false)

	p := m.MustGetPage("front", "home")

	tplLen := len(p.Tpls)
	if tplLen != 2 {
		t.Error("TPLS count. Expected 2, got", tplLen)
	}

	base := "front/base.html"
	if p.Tpls[0] != base {
		t.Error("Base was not properly interpolated.")
	}

	if p.ID != "home" {
		t.Error("ID wrong expected: home got:", p.ID)
	}
}

func TestJSPathDev(t *testing.T) {
	m := MustDecodeManifest(MANIFEST, false)

	expectedPath := "/js/vendor/jquery.js?v=0"
	resultPath := m.GetJsPath("jquery")

	if resultPath != expectedPath {
		t.Error("JS Path. Expected", expectedPath, "Result", resultPath)
	}
}

func TestJSPathProd(t *testing.T) {
	m := MustDecodeManifest(MANIFEST, true)

	expectedPath := "/js/vendor/jquery.min.js?v=0"
	resultPath := m.GetJsPath("jquery")

	if resultPath != expectedPath {
		t.Error("JS Path. Expected", expectedPath, "Result", resultPath)
	}
}

func TestJSPathProdComputed(t *testing.T) {
	m := MustDecodeManifest(MANIFEST, true)

	expectedPath := "/js/app.min.js?v=0"
	resultPath := m.GetJsPath("app")

	if resultPath != expectedPath {
		t.Error("JS Path. Expected", expectedPath, "Result", resultPath)
	}
}

func TestCSSProdNoMin(t *testing.T) {
	m := MustDecodeManifest(MANIFEST, true)

	expectedPath := "/css/app.css?v=0"
	resultPath := m.GetCssPath("app")

	if resultPath != expectedPath {
		t.Error("CSS Path. Expected", expectedPath, "Result", resultPath)
	}
}
