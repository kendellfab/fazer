// Fazer provides a template caching and rendering solution.
package fazer

import (
	"bytes"
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"path/filepath"

	"github.com/gorilla/sessions"
)

const (
	Flash        = "flash"
	FlashError   = "flasherror"
	FlashSuccess = "flashsuccess"
)

// Merge is a function that allows the user to register a method that will be called previous to rendering.
// This is useful if you want to inspect the path and add data to the rendering context.
type Merge func(r *http.Request, data map[string]interface{})

// Fazer holds the information required to lookup, cache, and render html templates to be output in a web request.
type Fazer struct {
	sessions.Store
	templateFiles map[string][]string
	templateCache map[string]*template.Template
	tplDir        string
	tplFuncs      map[string]interface{}
	config        interface{}
	prod          bool
	manifest      *Manifest
	merge         Merge
}

// NewFazer provides the fazer renderer with all required things initialized, including sane template functions.
// store: Gorilla sessions store.  This is used for adding and displaying flash messages.
// tplDir: This is the directory in the filesystem that contains the templates.  This directory can contain sub directories.
// config: This is any struct that you want to contain config values needed for rendering templates
// prod: Tells the system if you're running in production.  This affects the caching and loading.
func NewFazer(store sessions.Store, tplDir string, config interface{}, prod bool) *Fazer {
	f := &Fazer{
		Store:         store,
		templateFiles: make(map[string][]string),
		templateCache: make(map[string]*template.Template),
		tplDir:        tplDir,
		tplFuncs:      make(map[string]interface{}),
		config:        config,
		prod:          prod,
	}

	f.tplFuncs["partial"] = f.Partial
	f.tplFuncs["safeHtml"] = SafeHTML
	f.tplFuncs["truncate"] = Truncate
	f.tplFuncs["md"] = MRender
	f.tplFuncs["hasTrim"] = HasTrim
	f.tplFuncs["trim"] = Trim
	f.tplFuncs["xurls"] = XUrls

	return f
}

// AddManifest registers a manifest with the fazer renderer.  This automatically registers the js, css, and img functions with the template.
func (f *Fazer) AddManifest(manifest *Manifest) error {
	var err error
	f.manifest = manifest
	f.RegisterTemplateFunc("js", manifest.GetJsPath)
	f.RegisterTemplateFunc("css", manifest.GetCssPath)
	f.RegisterTemplateFunc("img", manifest.GetImgPath)
	for pg := range manifest.IteratePages() {
		if tplErr := f.RegisterTemplate(pg.path, pg.Tpls...); tplErr != nil {
			err = tplErr
		}
	}
	for key, tpl := range manifest.Partials {
		f.RegisterTemplate(key, tpl)
	}
	return err
}

// RegisterTemplate adds a template list to the renderer.
func (f *Fazer) RegisterTemplate(key string, templates ...string) error {

	// Prep the template strings to start with the tplDir
	var list []string
	for _, t := range templates {
		list = append(list, filepath.Join(f.tplDir, t))
	}

	f.templateFiles[key] = list
	if f.prod {
		if tpl, err := f.parseTemplate(key, list...); err == nil {
			f.templateCache[key] = tpl
		} else {
			return err
		}
	}
	return nil
}

// RegisterMerge adds a merge function to the renderer.  This will be called previous to rendering a template.
func (f *Fazer) RegisterMerge(merge Merge) {
	f.merge = merge
}

// RenderTemplate renders a registered html template with an http.StatusOk
// data: is a map that will be passed to the template rendering.
// key: this is the key used to register the template group.  Will be of form section.page if registered from the manifest, see above.
// Calls RenderTemplateWithCode internally
func (f *Fazer) RenderTemplate(w http.ResponseWriter, r *http.Request, data map[string]interface{}, key string) {
	f.RenderTemplateWithCode(w, r, http.StatusOK, data, key)
}

// RenderTemplateWithCode renders a registered html template with the given http status code.
// data: is a map that will be passed to the template rendering.
// key: this is the key used to register the template group.  Will be of form section.page if registered from the manifest, see above.
// This creates a new map[string]interface{} if the passed in data is nil.
// The fazer.config interface{} is added under Config
// The *http.Request is added under Request
// If a page exists from the manifest with the given key, then it is added under Page
// FlashError is added for any errors on the flash session
// FlashSuccess is added for any success on the flash session
func (f *Fazer) RenderTemplateWithCode(w http.ResponseWriter, r *http.Request, code int, data map[string]interface{}, key string) {
	tpl, err := f.acquireTemplate(key)
	if err != nil {
		log.Println("error loading template", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error loading template"))
		return
	}

	if data == nil {
		data = make(map[string]interface{})
	}

	data["Config"] = f.config
	data["Request"] = r

	if f.manifest != nil {
		if page, ok := f.manifest.LookupPage(key); ok {
			data["Page"] = page
		}
	}

	data["FlashError"], data["FlashSuccess"] = f.GetFlashes(w, r)

	if f.merge != nil {
		f.merge(r, data)
	}

	var doc bytes.Buffer
	err = tpl.Execute(&doc, data)
	if err == nil {
		w.WriteHeader(code)
		w.Write(doc.Bytes())
	} else {
		log.Println("Error executing templates", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error rendering templates"))
	}
}

// RenderJson is a shortcut method for rendering json to the response
func (f *Fazer) RenderJson(w http.ResponseWriter, r *http.Request, data interface{}) {
	if data, err := json.Marshal(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error rendering json"))
	} else {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
	}
}

func (f *Fazer) acquireTemplate(key string) (*template.Template, error) {
	if f.prod {
		if tpl, ok := f.templateCache[key]; ok {
			return tpl, nil
		} else {
			return nil, ErrorNoTemplate
		}
	}
	return f.parseTemplate(key, f.templateFiles[key]...)
}

func (f *Fazer) parseTemplate(key string, tpls ...string) (*template.Template, error) {
	return template.New(filepath.Base(tpls[0])).Funcs(f.tplFuncs).ParseFiles(tpls...)
}

func (f *Fazer) RegisterTemplateFunc(key string, fn interface{}) {
	f.tplFuncs[key] = fn
}

// Redirect returns a redirect with http.StatusSeeOther
func (f *Fazer) Redirect(w http.ResponseWriter, r *http.Request, url string) {
	f.RedirectCode(w, r, url, http.StatusSeeOther)
}

// RedirectCode returns a redirect with given http status
func (f *Fazer) RedirectCode(w http.ResponseWriter, r *http.Request, url string, code int) {
	http.Redirect(w, r, url, code)
}

// SetErrorFlash sets a flash error message if the fazer.store is not nil
func (f *Fazer) SetErrorFlash(w http.ResponseWriter, r *http.Request, message string) {
	f.setFlashMessage(w, r, FlashError, message)
}

// SetSuccessFlash sets a flash success message if the fazer.store is not nil
func (f *Fazer) SetSuccessFlash(w http.ResponseWriter, r *http.Request, message string) {
	f.setFlashMessage(w, r, FlashSuccess, message)
}

func (f *Fazer) setFlashMessage(w http.ResponseWriter, r *http.Request, key, message string) {
	if f.Store == nil {
		return
	}
	if sess, sessErr := f.Get(r, Flash); sessErr == nil {
		sess.AddFlash(message, key)
		sess.Save(r, w)
	}
}

// GetFlashes returns any error or success flashes that are on the session.  Usually only to be used internally
func (f *Fazer) GetFlashes(w http.ResponseWriter, r *http.Request) ([]interface{}, []interface{}) {
	if f.Store == nil {
		return nil, nil
	}

	var errFlash []interface{}
	var successFlash []interface{}
	if sess, sessErr := f.Get(r, Flash); sessErr == nil {
		errFlash = sess.Flashes(FlashError)
		successFlash = sess.Flashes(FlashSuccess)
		sess.Options.MaxAge = -1
		sess.Save(r, w)
	}
	return errFlash, successFlash
}

// Partial allows a partial to be loaded within the template.
// Use: {{ partial "key" . }}
func (f *Fazer) Partial(name string, payload interface{}) (template.HTML, error) {
	var buff bytes.Buffer

	tpl, loadErr := f.acquireTemplate(name)
	if loadErr != nil {
		return "", loadErr
	}

	execErr := tpl.Execute(&buff, payload)

	if execErr != nil {
		return "", execErr
	}

	return template.HTML(string(buff.Bytes())), nil
}
