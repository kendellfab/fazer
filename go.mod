module gitlab.com/kendellfab/fazer

go 1.12

require (
	github.com/gorilla/sessions v1.1.3
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/mvdan/xurls v1.1.0 // indirect
	github.com/pelletier/go-toml v1.8.1
	github.com/pkg/errors v0.8.1
	github.com/yuin/goldmark v1.1.9
	mvdan.cc/xurls v1.1.0
)
