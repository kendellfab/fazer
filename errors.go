package fazer

import "github.com/pkg/errors"

var ErrorNoTemplate = errors.New("no template found")
